import { BusFactory } from '../factories/bus.factory';
import { MotorCycleFactory } from '../factories/motor-cycle.factory';
import { PassengerCarFactory } from '../factories/passenger-car.factory';
import { TrailerFactory } from '../factories/trailer.factory';
import { TruckFactory } from '../factories/truck.factory';
import { VehicleRepository } from '../repositories/vehicle.repository';

export class Seed {
    private passengerCarFactory: PassengerCarFactory;
    private busFactory: BusFactory;
    private motorcycleFactory: MotorCycleFactory;
    private truckFactory: TruckFactory;
    private trailerFactory: TrailerFactory;

    constructor(private readonly vechicleRepository: VehicleRepository) {
        this.passengerCarFactory = new PassengerCarFactory();
        this.busFactory = new BusFactory();
        this.motorcycleFactory = new MotorCycleFactory();
        this.truckFactory = new TruckFactory();
        this.trailerFactory = new TrailerFactory();
    }

    public seedVehicles(): void {
        this.vechicleRepository.add(this.passengerCarFactory.create())
        this.vechicleRepository.add(this.truckFactory.create());
        this.vechicleRepository.add(this.busFactory.create());
        this.vechicleRepository.add(this.busFactory.create());
    }
}