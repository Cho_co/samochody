export enum LendStatus{
    ReadyToBorrow,
    Borrowed,
    Unvailable,
}