import { Vehicle } from './vehicle.model';

export class Motorcycle extends Vehicle {
    public hasTrunk: boolean;
    public engineCapacity: number;
}
