
import { DriverLicense } from '../enums/driver-license.enum';
import { LendStatus } from '../enums/lend-status.enum';
import { VehicleBrand } from '../enums/vehicle-brand.enum';
import { VehicleModel } from '../enums/vehicle-model.enum';
import { VehicleType } from '../enums/vehicle-type.enum';
import { ILendable } from '../Interfaces/interface-ilendable';
import { Trailer } from './trailer.model';

export abstract class Vehicle implements ILendable {
  public type: VehicleType;
  public brand: VehicleBrand;
  public model: VehicleModel;
  public traveledKilometers: number;
  public passengersCount: number;
  public trailer: Trailer;
  public requiredDriverLicense: DriverLicense;
  public lendStatus: LendStatus;
  public lendCost: number;
}




