import { Client } from './client.model';
import { Vehicle } from './vehicle.model';
import { LoanStatus } from '../enums/loan-status.enum';

export class LoanInfo {
    public loanStatus: LoanStatus;
    public borrowDate: Date;
    public costPerKilometer: number;
    public additionalInsurance: boolean;
    public client: Client;
    public vehicle: Vehicle;
    public notes: string;
}

