import { HookType } from '../enums/hook-type.enum';
import { LendStatus } from '../enums/lend-status.enum';
import { ILendable } from '../Interfaces/interface-ilendable';

export class Trailer implements ILendable {
    public maxOverload: number;
    public tiresCount: number;
    public hookType: HookType;
    public lendStatus: LendStatus;
    public lendCost: number;
}
