export abstract class Repository<T>{
    protected items: T[] = [];

    public getAll(): T[] {
        return this.items;
    }
    public add(item: T): void {
        this.items.push(item);
    }
}