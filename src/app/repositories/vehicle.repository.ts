import { Repository } from './repository';
import { Vehicle } from '../models/vehicle.model';
import { VehicleType } from '../enums/vehicle-type.enum';

export class VehicleRepository extends Repository<Vehicle>{
    
    public getByType(type: VehicleType): Vehicle[] {
        return this.items.filter(s => s.type == type);
    }
}
