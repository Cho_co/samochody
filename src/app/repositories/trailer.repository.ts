import { Trailer } from '../models/trailer.model';
import { Repository } from './repository';
import { HookType } from '../enums/hook-type.enum';

export class TrailerRepository extends Repository<Trailer>{
    
    public getByHookType(hookType: HookType): Trailer[] {
        return this.items.filter(t => t.hookType == hookType);
    }
}
