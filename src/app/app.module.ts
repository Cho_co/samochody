import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { CarSharingBaseModule } from './car-sharing-base/car-sharing-base.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CarSharingBaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
