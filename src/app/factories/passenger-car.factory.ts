
import { PassengerCar } from '../models/passenger-car.model';
import { VehicleFactory } from './vehicle-factory';
import { VehicleType } from '../enums/vehicle-type.enum';

export class PassengerCarFactory extends VehicleFactory{

    public create(): PassengerCar {
        let newPassengerCar = new PassengerCar();
        newPassengerCar.type = VehicleType.PassengerCar;
        this.fillVehicleBaseData(newPassengerCar);
        newPassengerCar.hasGps = Boolean(this.getRandomDigit(0, 1));
        return newPassengerCar;
    }
}