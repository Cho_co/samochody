import { VehicleBrand } from '../enums/vehicle-brand.enum';
import { VehicleModel } from '../enums/vehicle-model.enum';
import { VehicleType } from '../enums/vehicle-type.enum';
import { Vehicle } from '../models/vehicle.model';
import { TrailerFactory } from './trailer.factory';

export abstract class VehicleFactory {

    public fillVehicleBaseData(vehicle: Vehicle): void {
        vehicle.brand = this.getRandomDigit(1, 5);
        vehicle.passengersCount = this.getRandomDigit(20, 70);
        this.chooseRequiredDriverLicense(vehicle.type, vehicle);
        vehicle.traveledKilometers = this.getRandomDigit(1, 1000000);
        vehicle.model = this.chooseModel(vehicle.brand);
        let newtrailerFactory: TrailerFactory = new TrailerFactory();
        let newTrailer = newtrailerFactory.create();
        vehicle.trailer = newTrailer;
    }

    public getRandomDigit(from: number, to: number): number {
        return Math.floor(Math.random() * (to - from) + from);
    }

    private chooseModel(vechicleBrand: VehicleBrand): VehicleModel {
        let vechicleModel;
        switch (vechicleBrand) {
            case VehicleBrand.Scania: vechicleModel = this.getRandomDigit(1, 5);
                break;
            case VehicleBrand.Solaris: vechicleModel = this.getRandomDigit(5, 9);
                break;
            case VehicleBrand.Mercedes: vechicleModel = this.getRandomDigit(9, 13);
                break;
            case VehicleBrand.Volvo: vechicleModel = this.getRandomDigit(13, 17);
                break;
            default: console.error("Wrong Brand");
        }
        return vechicleModel;
    }

    public chooseRequiredDriverLicense(vehicleType: VehicleType, vechicle: Vehicle) {
        switch (vehicleType) {
            case VehicleType.Bus: vechicle.requiredDriverLicense = this.getRandomDigit(1, 5);
                break;
            case VehicleType.Motocycle: vechicle.requiredDriverLicense = this.getRandomDigit(5, 9);
                break;
            case VehicleType.PassengerCar: vechicle.requiredDriverLicense = this.getRandomDigit(9, 13);
                break;
            case VehicleType.Truck: vechicle.requiredDriverLicense = this.getRandomDigit(13, 17);
                break;
            default: console.error("Wrong driver license, cant pick up");
        }
    }
}
