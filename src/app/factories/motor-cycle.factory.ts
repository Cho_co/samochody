import { Motorcycle } from '../models/motorcycle.model';
import { VehicleFactory } from './vehicle-factory';
import { VehicleType } from '../enums/vehicle-type.enum';

export class MotorCycleFactory extends VehicleFactory{

    public create(): Motorcycle {
        let newMotorCycle = new Motorcycle();
        newMotorCycle.type = VehicleType.Motocycle;
        this.fillVehicleBaseData(newMotorCycle);
        newMotorCycle.engineCapacity = this.getRandomDigit(200,1000);
        newMotorCycle.hasTrunk = Boolean(this.getRandomDigit(0, 1));
        return newMotorCycle;
    }
}