import { Bus } from '../models/bus.model';
import { VehicleFactory } from './vehicle-factory';
import { VehicleType } from '../enums/vehicle-type.enum';

export class BusFactory extends VehicleFactory {

    public create(): Bus {
        let newBus = new Bus();
        newBus.doorsCount = this.getRandomDigit(1,3);
        newBus.type = VehicleType.Bus;
        this.fillVehicleBaseData(newBus);
        return newBus;
    }
}



