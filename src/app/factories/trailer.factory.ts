import { Trailer } from '../models/trailer.model';

export class TrailerFactory {
    
    public create(): Trailer {
        let newTrailer = new Trailer();
        newTrailer.hookType = this.getRandomDigit(0, 3);
        newTrailer.maxOverload = this.getRandomDigit(1, 50);
        newTrailer.tiresCount = this.getRandomDigit(1, 8);
        newTrailer.lendCost = this.getRandomDigit(500, 1500);
        newTrailer.lendStatus = this.getRandomDigit(0, 1);
        return newTrailer;
    }

    public getRandomDigit(from: number, to: number): number {
        return Math.floor(Math.random() * (to - from) + from);
    }
}