import { Truck } from '../models/truck.model';
import { VehicleFactory } from './vehicle-factory';
import { VehicleType } from '../enums/vehicle-type.enum';

export class TruckFactory extends VehicleFactory {

    public create(): Truck {
        let newTruck = new Truck();
        this.fillVehicleBaseData(newTruck);
        newTruck.type = VehicleType.Truck;
        newTruck.hasBedroom = Boolean(this.getRandomDigit(0, 1));
        return newTruck;
    }
}