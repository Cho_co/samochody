import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { CarFormComponent } from './car-form/car-form.component';
import { CarListComponent } from './car-list/car-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { ClientsFormComponent } from './clients-form/clients-form.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { HomeComponent } from './home/home.component';
import { LoanDesktopComponent } from './loan-desktop/loan-desktop.component';
import { LoanDetailsComponent } from './loan-details/loan-details.component';
import { LoanFooterComponent } from './loan-footer/loan-footer.component';
import { LoanFormComponent } from './loan-form/loan-form.component';
import { LoanHeaderComponent } from './loan-header/loan-header.component';
import { LoanListComponent } from './loan-list/loan-list.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { NewLoanComponent } from './new-loan/new-loan.component';

const appRoutes: Routes = [
  { path: 'loan-desktop', component: LoanDesktopComponent },
  { path: 'car-list', component: CarListComponent },
  { path: 'clients-list', component: ClientsListComponent },
  { path: 'loan-list', component: LoanListComponent },
  { path: 'new-loan', component: NewLoanComponent },
]

@NgModule({
  declarations: [
    HomeComponent,
    LoanHeaderComponent,
    LoanFooterComponent,
    NavigationBarComponent,
    CarListComponent,
    CarFormComponent,
    ClientsListComponent,
    ClientsFormComponent,
    ClientDetailsComponent,
    LoanListComponent,
    LoanFormComponent,
    LoanDetailsComponent,
    LoanDesktopComponent,
    NewLoanComponent
  ],
  exports: [HomeComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [HomeComponent]
})
export class CarSharingBaseModule { }
