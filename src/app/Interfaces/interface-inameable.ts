export interface INameable {
    getname(): String;
}