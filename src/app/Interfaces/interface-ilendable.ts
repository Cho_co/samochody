import { LendStatus } from '../enums/lend-status.enum';
import { INameable } from './interface-inameable';

export interface ILendable extends INameable{
    lendStatus: LendStatus;
    lendCost: number;
}