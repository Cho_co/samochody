import { Component, OnInit } from '@angular/core';
import { BusFactory } from './factories/bus.factory';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'Samochody';

  ngOnInit() {
    let busFactory: BusFactory = new BusFactory();
    let newmotor = busFactory.create();

    console.log(newmotor);
  }
}

