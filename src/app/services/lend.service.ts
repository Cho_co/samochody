import { Observable, Subject } from 'rxjs';
import { LendStatus } from '../enums/lend-status.enum';
import { ILendable } from '../Interfaces/interface-ilendable';

export class LendService {

    private onObjectLendedEvent: Subject<ILendable>;
    constructor() {
        this.onObjectLendedEvent = new Subject<ILendable>();
        console.log("Event Created");
    }
    public onObjectLended(): Observable<ILendable> {
        console.log("New Subscriber");
        return this.onObjectLendedEvent.asObservable();
    }
    public lend(object: ILendable): void {
        if (object.lendStatus != LendStatus.ReadyToBorrow) {
            throw new Error(`Cannot borrow object with status ${object.lendStatus}`);
        }

        object.lendStatus = LendStatus.Borrowed;
        console.log("Invoke Event");
        this.onObjectLendedEvent.next(object);
    }
    public finishLend(object: ILendable): void {
        if (object.lendStatus != LendStatus.Borrowed) {
            throw new Error(`Cannot finish lend for object with status${object.lendStatus}`)
        }
        
        object.lendStatus = LendStatus.ReadyToBorrow;
        this.onObjectLendedEvent.next(object);
    }
}