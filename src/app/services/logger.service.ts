import { LendService } from './lend.service';
import { ILendable } from '../Interfaces/interface-ilendable';
import { LendStatus } from '../enums/lend-status.enum';

export class LoggerService{
    public enabled: boolean = true;
    constructor(private lendService: LendService){
        this.subscribe();
    }
    private subscribe(): void {
        console.log("Subscribed");
        this.lendService.onObjectLended().subscribe(l => this.logObjectLended(l));
    }
    private logObjectLended(lendedObject: ILendable): void {
        
      if(lendedObject.lendStatus == LendStatus.Borrowed){
        console.log( lendedObject.getname() + " has been borrowed");
      }
      else {
        console.log(lendedObject.getname() + " has been returned");
    }
    
    }

    public log(message: string): void{
        if(this.enabled){
            console.log(message);
        }

    }
}